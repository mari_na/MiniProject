package com.example.demo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.swing.filechooser.FileNameExtensionFilter;
import javax.validation.Valid;

import org.jboss.logging.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Article;

import com.example.demo.service.ArticleServicable;
import com.example.demo.service.ArticleService;
@Controller
public class ArticleController {
	
	@Autowired
	private ArticleServicable articleServicable;
	
	@Autowired
	public void setArticleService(ArticleServicable articleServicable) {
		this.articleServicable = articleServicable;
	}
	@Bean
	public ArticleService articleService() {
		return new ArticleService();
	}
	
	@GetMapping({"/article","/",""})
	public String article(ModelMap m) {
		List<Article> articles = articleServicable.findAll();
		System.out.println(articles);
		m.addAttribute("articles",articles);
		return "article";
	}

	@GetMapping("/add")
	public String add(ModelMap m) {
		m.addAttribute("article", new Article());
		m.addAttribute("formAdd",true);
		return "add";
	}
	
	@GetMapping("/update/{id}")
	public String update(@PathVariable int id, ModelMap m) {
		m.addAttribute("article", articleServicable.findOne(id));
		m.addAttribute("formAdd",false);
		return "add";
	}
	
	@PostMapping("/update")
	public String saveUpdate(@Valid @ModelAttribute Article article,BindingResult result,ModelMap m, @RequestParam("file") MultipartFile file) {
		article.setDate(new Date().toString());
		if(result.hasErrors()){
			m.addAttribute("article", article);
			m.addAttribute("formAdd",false);
			return "add";
		}
		String fileName="";
		String pathFile="/Users/acer/Desktop/image/";
		if(!file.isEmpty()) {
			try {
				fileName = UUID.randomUUID()+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
				Files.copy(file.getInputStream(), Paths.get(pathFile, fileName));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		article.setThumnailURL("/image/"+fileName);
		articleServicable.update(article);
		return "redirect:/article";
	}
	
	@PostMapping("/add")
	public String save(@Valid @ModelAttribute Article article,BindingResult result,ModelMap m, @RequestParam("file") MultipartFile file) {
		article.setDate(new Date().toString());
		if(result.hasErrors()){
			m.addAttribute("article", article);
			m.addAttribute("formAdd",true);
			return "add";
		}
		String fileName="";
		String pathFile="/Users/acer/Desktop/image/";
		if(!file.isEmpty()) {
			try {
				fileName = UUID.randomUUID()+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
				Files.copy(file.getInputStream(), Paths.get(pathFile, fileName));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		article.setThumnailURL("/image/"+fileName);
		articleServicable.add(article);
		return "redirect:/add";
	}
	
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") int id) {
		articleServicable.delete(id);
		return "redirect:/article";
	}
	@GetMapping("/view/{id}")
	public String view(ModelMap m, @PathVariable("id") int id) {
		m.addAttribute("article", articleServicable.findOne(id));
		return "view";
	}
}
