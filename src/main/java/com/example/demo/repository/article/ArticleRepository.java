package com.example.demo.repository.article;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.demo.model.Article;
import com.github.javafaker.Faker;

@Repository
public class ArticleRepository implements ArticleRepositorable {

	private List<Article> articles = new ArrayList<>();

	public ArticleRepository() {
		Faker faker = new Faker();
		for (int i=0 ; i<5 ; i++) {
			articles.add(new Article(i, faker.book().title(), faker.book().author(), new Date().toString(),faker.internet().image()));	
		}
		
	}

	@Override
	public void add(Article article) {
		articles.add(article);

	}

	@Override
	public Article findOne(int id) {
		for (Article article : articles) {
			if (article.getId() == id) {
				return article;
			}
		}
		return null;
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		for (Article article : articles) {
			if (article.getId() == id) {
				articles.remove(article);
				return;
			}
		}
	}
	@Override
	public List<Article> findAll() {
		// TODO Auto-generated method stub
		return articles;
	}

	@Override
	public void update(Article article) {
		// TODO Auto-generated method stub
		for(int i=0 ; i<articles.size() ; i++) {
			if(articles.get(i).getId()==article.getId()) {
				articles.get(i).setName(article.getName());
				articles.get(i).setAuthor(article.getAuthor());
				articles.get(i).setThumnailURL(article.getThumnailURL());
				return;
			}
		}
	}

}
