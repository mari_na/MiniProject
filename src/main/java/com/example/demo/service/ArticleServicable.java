package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Article;

public interface ArticleServicable {
	void add(Article article);
	Article findOne(int id);
	List<Article> findAll();
	void delete(int id);
	void update(Article article);
}
