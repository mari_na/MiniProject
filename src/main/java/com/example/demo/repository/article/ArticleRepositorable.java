package com.example.demo.repository.article;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Article;
@Repository
@MapperScan("com.example.demo.repository")
public interface ArticleRepositorable {
	@Insert("INSERT INTO tb_articles(title,author,createddate,thumbnail) VALUES(#{name},#{author},#{date},#{thumnailURL})")
	public void add(Article article);
	
	@Select("SELECT id,title,author,createddate,thumbnail from tb_articles ")
	@Results({
		@Result(property="id", column="id"),
		@Result(property="name", column="title"),
		@Result(property="author", column="author"),
		@Result(property="date", column="createddate"),
		@Result(property="thumnailURL", column="thumbnail"),
	})
	public Article findOne(int id);
	
	@Select("SELECT id,title,author,createddate,thumbnail from tb_articles ")
	@Results({
		@Result(property="id", column="id"),
		@Result(property="name", column="title"),
		@Result(property="author", column="author"),
		@Result(property="date", column="createddate"),
		@Result(property="thumnailURL", column="thumbnail"),
	})
	public List<Article> findAll();
	@Delete("DELETE FROM tb_articles WHERE id=#{id}")
	public void delete(int id);
	
	@Update("UPDATE tb_articles SET title=#{name}, author=#{author} WHERE id=#{id}")
	public void update(Article article);
}
